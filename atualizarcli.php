
<?php
session_start();
// if (!$_SESSION['idUsuario']) header("Location: index.html");

$id_usuario = $_GET['id'];

if (isset($_POST["submit"])) {
    $nome = $_POST['nome'];
    $email = $_POST['email'];
    $cpf = $_POST['cpf'];
    $login = $_POST['login'];
    $senha = $_POST['senha'];
    // return;
}

if ($db = mysqli_connect('localhost', 'root', '', 'subphp', 3307)) { } else {
    die("Problema ao conectar ao SGDB");
}

if (!empty($login)) {

    if(!empty($senha)){
        $p = mysqli_prepare($db, 'UPDATE cliente SET nome = ?, email = ?, cpf = ?, login = ? WHERE id = ?');

        mysqli_stmt_bind_param($p, 'ssiss', $nome, $email, $cpf, $login, $id_usuario); mysqli_stmt_execute($p);
    }else{
        $p = mysqli_prepare($db, 'UPDATE cliente SET nome = ?, email = ?, cpf = ?, login = ? WHERE id = ?');

    mysqli_stmt_bind_param($p, 'ssiss', $nome, $email, $cpf, $login, $id_usuario);
    mysqli_stmt_execute($p);
    }
    
}

$p = mysqli_prepare($db, 'SELECT * FROM cliente WHERE id= ?');
mysqli_stmt_bind_param($p, 's', $id_usuario);

mysqli_stmt_execute($p);

$result = mysqli_stmt_get_result($p);
$usuario = mysqli_fetch_assoc($result);

?>


<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Crud PHP</title>
    <link href="https://fonts.googleapis.com/css?family=Euphoria+Script|Montserrat:400,500,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/styleli.css">
    <script src=javascript/script.js></script>
</head>

<body>        
<header id="menu-bg">
        <nav class="menu">
            <ul>
                <li><a href="formcliente.php">Adicionar Cliente</a></li>
                <li><a href="formaero.php">Adicionar Aeronaves</a></li>

                   <li><a href="#">Listagens</a>
                    <ul>
                        <li><a href="listaraero.php">Aeronaves</a></li>
                        <li><a href="listarcli.php">Clientes</a></li>
                    </ul>
                </li>
                            </ul>
        </nav>                 
    </header>
    <main class="main cadastro-user">

        <div class="cad-background">
            <h2>Editar Usuário</h2>
        </div>

        <form action="atualizarcli.php?id=<?=$id_usuario?>" class="formulario" method="POST">

            <div class="form-container">

                <div class="aba-pessoais">
                    <h3 class="subtitulo">Dados Pessoais</h3>

                    <div class="linha-um">
                        <span class="label-nome">Nome Completo</span>
                        <input class="campo-nome" name="nome" type="text" value="<?= $usuario['nome'] ?>">
                    </div>

                    <div class="linha-tres">
                        <div class="linha-dupla">
                            <span class="label">Email</span>
                            <input class="campo" name="email" type="text" value="<?= $usuario['email'] ?>">
                        </div>

                        <div class="linha-dupla">
                            <span class="label">CPF</span>
                            <input class="campo" name="cpf" type="text" value="<?= $usuario['cpf'] ?>">
                        </div>
                    </div>

                    
                       
                    </div>
                </div>

                    <h3 class="subtitulo">Login e Senha</h3>

                    <div class="linha-sete">
                        <div class="linha-dupla">
                            <span class="label">Login</span>
                            <input class="campo" name="login" type="text" value="<?= $usuario['login'] ?>">
                        </div>
                        <div class="linha-dupla">
                            <span class="label">Senha</span>
                            <input class="campo" name="senha" type="password">
                        </div>
                    </div>
                </div>

            </div>

            <div class="user-btn">
                <button onclick="return atualizarFunction()"  class="submit" name="submit" type="submit">Atualizar usuário</button>
                <a href="listarcli.php" class="reset" type="reset">Cancelar</a>
            </div>


            <script>
                            function atualizarFunction() {
                            var r = confirm("Quer mesmo atualizar?");
                            if (r == false) {
                                        return false;
                                                            } 

                                                            }
                                        </script>


        </form>
    </main>

</body>

</html>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Crud PHP</title>
    <link href="https://fonts.googleapis.com/css?family=Euphoria+Script|Montserrat:400,500,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/styleli.css">
    <script src=javascript/script.js></script>
</head>

<body>        
<header id="menu-bg">
        <nav class="menu">
            <ul>
                <li><a href="formcliente.php">Adicionar Cliente</a></li>
                <li><a href="formaero.php">Adicionar Aeronaves</a></li>

                   <li><a href="#">Listagens</a>
                    <ul>
                        <li><a href="listaraero.php">Aeronaves</a></li>
                        <li><a href="listarcli.php">Clientes</a></li>
                    </ul>
                </li>
                            </ul>
        </nav>                 
    </header>
    
    
    <span class="logo-crud">Crud</span>
    <div class="menu-sobreposicao"></div>   
    
    <main>        
        <a class="btn-produtos" href="formaero.php">Cadastrar Aeronave</a>
        </div>

        <section class="tabela-produtos">

            <table class="tabela">

                <thead>
                    <tr>
                        <!-- <th class="checkbox"><input type="checkbox"></th> -->
                        
                        <th class="rotulo">ID</th>
                        <th class="rotulo">ID Cliente</th>
                        <th class="rotulo">Matricula</th>
                        <th class="rotulo">Modelo</th>
                        <th class="rotulo">Ano</th>
                        <th class="rotulo">Cor</th>
                       
                    </tr>
                </thead>
                <tbody>

                    <?php

                    if ($db = mysqli_connect('localhost', 'root', '', 'subphp', 3307)) {
                        // Nada dentro
                    } else {
                        die("Problema ao conectar ao SGDB");
                    }

                    $p = mysqli_prepare($db, '	SELECT * FROM aeronave');
                    mysqli_stmt_execute($p);
                    $result = mysqli_stmt_get_result($p);

                    while ($aeronave = mysqli_fetch_assoc($result)) {
                        ?>

                        <tr class="row">
                            <!-- <td><input name="checkProd" type="checkbox"></td> -->
                            <td><?= $aeronave['id'] ?></td>
                            <td><?= $aeronave['id_cliente'] ?></td>
                            <td><?= $aeronave['matricula'] ?></td>
                            <td><?= $aeronave['modelo'] ?></td>
                            <td><?= $aeronave['ano'] ?></td>
                            <td><?= $aeronave['cor'] ?></td>
                            
                            <td>
                                <a href="atualizaraero.php?id=<?= $aeronave['id'] ?>"><img class="icones" src="img/edit.png" alt=""></a>
                                <a href="excluiraero.php?id=<?= $aeronave['id'] ?>"><img class="icones" src="img/delete.png" alt=""></a>
                            </td>

                        </tr>

                    <?php
                    }
                    ?>




                </tbody>

            </table>
        </section>

    </main>




</html>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Aula php</title>
    <link href="https://fonts.googleapis.com/css?family=Euphoria+Script|Montserrat:400,500,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/styleadd.css">
    <script src=javascript/script.js></script>
</head>

<body>        
<header id="menu-bg">
        <nav class="menu">
            <ul>
                <li><a href="formcliente.php">Adicionar Cliente</a></li>
                <li><a href="formaero.php">Adicionar Aeronaves</a></li>

                   <li><a href="#">Listagens</a>
                    <ul>
                        <li><a href="listaraero.php">Aeronaves</a></li>
                        <li><a href="listarcli.php">Clientes</a></li>
                    </ul>
                </li>
                            </ul>
        </nav>                 
    </header>
    
    
    <span class="logo-crud">Crud</span>
    <div class="menu-sobreposicao"></div>   
    
    <main>     
        <h1 class="agende-h1">Adicionar cliente</h1>
        <form action="addaero.php" class="formulario" method="POST">
        
        <div class="aba-pessoais">
                    <h3 class="subtitulo">Aeronaves</h3>

                    <div class="linha-um">
                   <div> <span class="label-nome">Id Cliente</span>
                        <input class="campo-nome" name="id_cliente" type="text">
                    </div>
                    <div>
                        <span class="label-nome">Matricula</span>
                        <input class="campo-nome" name="matricula" type="text">
                    </div>

                    <div class="linha-dois">
                        <div class="linha-dupla">
                            <span class="label">Modelo</span>
                            <input class="campo" name="modelo" type="text">
                        </div>
                        <div class="linha-dupla">
                            <span class="label">Ano</span>
                            <input class="campo" name="ano" type="text">
                        </div>
                    </div>

                    <div class="linha-tres">
                        <div class="linha-dupla">
                            <span class="label">Cor</span>
                            <input class="campo" name="cor" type="text">
                        </div>
                        
                    </div>
                </div>
            <div class="prod-btn">
                    <button class="submit" type="submit">Adicionar produto</button>
                    <a class="reset" href="listarcli.php">Cancelar</a>
            
            </div>
        </form>
     



</html>
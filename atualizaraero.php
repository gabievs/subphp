
<?php
session_start();
// if (!$_SESSION['idUsuario']) header("Location: index.html");

$id_usuario = $_GET['id'];

if (isset($_POST["submit"])) {
    $idcliente=$_POST['id_cliente'];
    $matricula = $_POST['matricula'];
$modelo = $_POST['modelo'];
$ano = $_POST['ano'];
$cor = $_POST['cor'];
    // return;
}

if ($db = mysqli_connect('localhost', 'root', '', 'subphp', 3307)) { } else {
    die("Problema ao conectar ao SGDB");
}

if (!empty($idcliente)) {

    if(!empty($matricula)){
        $p = mysqli_prepare($db, 'UPDATE aeronave SET id_cliente = ?, matricula = ?, modelo = ?, ano = ?,cor = ? WHERE id = ?');

        mysqli_stmt_bind_param($p, 'issysi', $idcliente, $matricula, $modelo, $ano,$cor, $id_usuario); 
        mysqli_stmt_execute($p);
    }else{
        $p = mysqli_prepare($db, 'UPDATE aeronave SET id_cliente = ?, matricula = ?, modelo = ?, ano = ?,cor = ? WHERE id = ?');

    mysqli_stmt_bind_param($p, 'issysi', $idcliente, $matricula, $modelo, $ano, $cor,$id_usuario);
    mysqli_stmt_execute($p);
    }
    
}

$p = mysqli_prepare($db, 'SELECT * FROM aeronave WHERE id= ?');
mysqli_stmt_bind_param($p, 's', $id_usuario);

mysqli_stmt_execute($p);

$result = mysqli_stmt_get_result($p);
$usuario = mysqli_fetch_assoc($result);

?>


<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Crud PHP</title>
    <link href="https://fonts.googleapis.com/css?family=Euphoria+Script|Montserrat:400,500,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/styleli.css">
    <script src=javascript/script.js></script>
</head>

<body>        
<header id="menu-bg">
        <nav class="menu">
            <ul>
                <li><a href="formcliente.php">Adicionar Cliente</a></li>
                <li><a href="formaero.php">Adicionar Aeronaves</a></li>

                   <li><a href="#">Listagens</a>
                    <ul>
                        <li><a href="listaraero.php">Aeronaves</a></li>
                        <li><a href="listarcli.php">Clientes</a></li>
                    </ul>
                </li>
                            </ul>
        </nav>                 
    </header>
    <main class="main cadastro-user">

        <div class="cad-background">
            <h2>Editar Usuário</h2>
        </div>

        <form action="atualizaraero.php?id=<?=$id_usuario?>" class="formulario" method="POST">

            <div class="form-container">

                <div class="aba-pessoais">
                    <h3 class="subtitulo">Dados Pessoais</h3>

                    <div class="linha-um">
                        <span class="label-nome">Id Cliente</span>
                        <input class="campo-nome" name="id_cliente" type="text" value="<?= $usuario['id_cliente'] ?>">
                    </div>

                    <div class="linha-tres">
                        <div class="linha-dupla">
                            <span class="label">Matricula</span>
                            <input class="campo" name="matricula" type="text" value="<?= $usuario['matricula'] ?>">
                        </div>

                        <div class="linha-dupla">
                            <span class="label">Modelo</span>
                            <input class="campo" name="modelo" type="text" value="<?= $usuario['modelo'] ?>">
                        </div>
                    </div>

                    
                       
                    </div>
                </div>

                    
                    <div class="linha-sete">
                        <div class="linha-dupla">
                            <span class="label">Ano</span>
                            <input class="campo" name="ano" type="text" value="<?= $usuario['ano'] ?>">
                        </div>
                        <div class="linha-dupla">
                        <span class="label">Cor</span>
                            <input class="campo" name="ano" type="text" value="<?= $usuario['cor'] ?>">
                        </div>
                    </div>
                </div>

            </div>

            <div class="user-btn">
                <button onclick="return atualizarFunction()"  class="submit" name="submit" type="submit">Atualizar</button>
                <a href="listaraero.php" class="reset" type="reset">Cancelar</a>
            </div>


            <script>
                            function atualizarFunction() {
                            var r = confirm("Quer mesmo atualizar?");
                            if (r == false) {
                                        return false;
                                                            } 

                                                            }
                                        </script>


        </form>
    </main>

</body>

</html>
